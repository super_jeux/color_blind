/**
 * \brief event file contain all code implementation to manage all event, mouse and keyboard include of SDL2
 *
 * \file BLV_event.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include "BLV_event.h"

SDL_bool BLV_manage_event(void)
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            return SDL_FALSE;
        case SDL_TEXTINPUT:
            SDL_Log("Text input : %s", event.text.text);
            break;
        case SDL_TEXTEDITING:
            SDL_Log("Text editing : %s", event.edit.text);
            break;
        case SDL_KEYDOWN:
            SDL_Log("Key down state : %u, repeat : %u et caractere : %c", event.key.state, event.key.repeat, event.key.keysym.sym);
            // change_player_speed(p, event.key.keysym.sym, event.key.state);
            break;
        case SDL_KEYUP:
            SDL_Log("Key up state : %u, repeat : %u et caractere : %c", event.key.state, event.key.repeat, event.key.keysym.sym);
            // change_player_speed(p, event.key.keysym.sym, event.key.state);
            break;
        }
    }

    return SDL_TRUE;
}