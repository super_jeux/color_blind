/**
 * \brief file contain all code implementation for manage a camera
 *
 * \file camera.c
 * \author Gautier Levesque
 * \date 06-02-2024
 */
#include "camera.h"

#include "BLV_graphic.h"

#include <math.h>

SDL_bool init_camera(Camera *c, uint16_t screen_width, uint16_t screen_height, uint16_t offset_x, uint16_t offset_y)
{
    c->speed_x = 0;
    c->speed_y = 0;
    c->pos.fill = SDL_FALSE;

    SDL_Color white = {255, 255, 255, 255};
    SDL_Rect rect = {0, 0, screen_width - offset_x, screen_height - offset_y};
    memcpy(&c->pos.color, &white, sizeof(SDL_Color));
    memcpy(&c->pos.r, &rect, sizeof(SDL_Rect));
    memcpy(&c->collider, &c->pos, sizeof(BLV_Rect));
    c->collider.r.w = 1000;
    c->collider.r.h = 680;
    c->collider.r.x = screen_width / 2.0 - c->collider.r.w / 2.0;
    c->collider.r.y = screen_height / 2.0 - c->collider.r.h / 2.0;

    return SDL_TRUE;
}

SDL_bool update_camera(Camera *c, Map m)
{
    SDL_Point mouse_position;
    SDL_GetMouseState(&mouse_position.x, &mouse_position.y);

    if (SDL_PointInRect(&mouse_position, &c->collider.r) == SDL_FALSE)
    {
        int dist_x = mouse_position.x - (c->collider.r.x + c->collider.r.w / 2.0);
        int dist_y = mouse_position.y - (c->collider.r.y + c->collider.r.h / 2.0);

        c->speed_x = 1 * dist_x / 10;
        c->speed_y = 1 * dist_y / 10;
        //SDL_Log("Camera speed : %d,%d", c->speed_x, c->speed_y);
    }
    else
    {
        c->speed_x = 0;
        c->speed_y = 0;
    }

    c->pos.r.x += c->speed_x;
    c->pos.r.y += c->speed_y;

    if (c->pos.r.x < 0)
        c->pos.r.x = 0;
    if (c->pos.r.y < 0)
        c->pos.r.y = 0;
    if (c->pos.r.x + c->pos.r.w >= m.width)
        c->pos.r.x = m.width - c->pos.r.w;
    if(c->pos.r.y + c->pos.r.h >= m.height)
        c->pos.r.y = m.height - c->pos.r.h;

    return SDL_TRUE;
}

SDL_bool draw_camera(SDL_Renderer *renderer, Camera c)
{
    return draw_rect(renderer, c.collider);
}
