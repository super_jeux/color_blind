/**
 * \brief file contain all code definition for launch and manage a SDL_Window with SDL2
 *
 * \file BLV_window.h
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#ifndef __BLV_WINDOW_H__
#define __BLV_WINDOW_H__

#include <SDL.h>

/**
 * \brief represent max size of car for a title SDL_Window
 */
#define TITLE_WINDOW_SIZE 255

/**
 * \brief use to represent a SDL_window with some parameter
 */
typedef struct
{
    SDL_Window *window;            /**<window> current SDL_Window*/
    int pos_x;                     /**<pos_x> x upper left corner of SDL_Window*/
    int pos_y;                     /**<pos_y> y upper left corner of SDL_Window*/
    int width;                     /**<width> width of SDL_Window*/
    int height;                    /**<height> height of SDL_Window*/
    char title[TITLE_WINDOW_SIZE]; /**<title> title of SDL_Window*/
    Uint32 flags;                  /**<flag> flag of SDL_Window*/
} BLV_Window;

/**
 * \brief use to create a BLV_Window
 *
 * \param path path to config file
 * \param w pointer to BLV_Window who will be filled in function
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool create_BLV_window(const char *path, BLV_Window *w);

/**
 * \brief use to proper clean a BLV_Window
 *
 * \param w pointer to BLV_Window already created
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool clean_BLV_window(BLV_Window *w);

#endif
