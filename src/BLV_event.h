/**
 * \brief event file contain all code defition to manage all event, mouse and keyboard include of SDL2
 *
 * \file BLV_event.h
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#ifndef __EVENT_H__
#define __EVENT_H__

#include <SDL.h>

#include "player.h"

/**
 * \brief use to manage all SDL2 event
 *
 * \return SDL_TRUE if we don't have a quit event or an error, SDL_FALSE instead
 */
SDL_bool BLV_manage_event(void);

#endif