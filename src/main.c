/**
 * \brief main file contain all code logic for start a window in SDL2
 *
 * \file main.c
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#include <SDL_image.h>
#include <time.h>

#include "BLV_window.h"
#include "BLV_graphic.h"
#include "BLV_event.h"
#include "BLV_draw.h"

#include "camera.h"
#include "map.h"
#include "player.h"

/**
 * \brief use to define how many FPS we want in process
 */
#define FPS 30

/**
 *\brief use to set render draw color to default
 */
static SDL_Color color_black = {0, 0, 0, 0};

/**
 * \brief use to draw all thing on screen
 *
 * \param renderer renderer to draw all thing
 * \param m map to draw
 * \param p unit of player to draw
 * \param c camera to draw with offset
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool BLV_draw(SDL_Renderer *renderer, Map m, Player_array *p, Camera c);

/**
 * \brief main function
 *
 * \param argc number of arugment passed to exe
 * \param argv string represent each argument
 *
 * \param return 0 on success, negative code on error
 */
int main(int argc, char *argv[])
{

    // to remove warning
    for (uint8_t i = 0; i < argc; ++i)
        printf("Argv[%d] = %s\r\n", i, argv[i]);

    // time variable
    Uint32 current_time = SDL_GetTicks();
    Uint32 past_time = SDL_GetTicks();
    const Uint32 fps = 1000 / FPS;

    srand(time(NULL));
    // core thing
    BLV_Window window;
    BLV_Renderer renderer;
    Map map;
    Player_array player_units;
    Camera camera;
    BLV_Texture background_texture;

    // initialization
    SDL_bool launch = SDL_TRUE;
    launch = launch && (SDL_Init(SDL_INIT_EVERYTHING) < 0 ? SDL_FALSE : SDL_TRUE);
    launch = launch && (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < 0 ? SDL_FALSE : SDL_TRUE);
    launch = launch && create_BLV_window("./config.txt", &window);
    launch = launch && create_BLV_renderer(window.window, &renderer);
    launch = launch && init_map(&map, 200, 200, 2000, 2000);
    launch = launch && init_array_of_player(&player_units, 50, map);
    launch = launch && init_camera(&camera, window.width, window.height, map.offset_x, map.offset_y);

    // bg36.png
    for (uint16_t i = 0; i < renderer.texture_count; ++i)
    {
        if (strcmp("bg36.png", renderer.texture_array[i].name) != 0)
            continue;

        background_texture.texture = renderer.texture_array[i].texture;
        background_texture.flip = SDL_FLIP_NONE;
        background_texture.center.x = 0;
        background_texture.center.y = 0;
        background_texture.src.x = 0;
        background_texture.src.y = 0;
        background_texture.src.w = 200;
        background_texture.src.h = 200;
        memcpy(background_texture.name, "bg36.png", strlen("bg36.png"));
    }

    // main loop
    while (launch == SDL_TRUE)
    {
        current_time = SDL_GetTicks();

        // if we need to update application
        if (current_time - past_time < fps)
            continue;

        launch = BLV_manage_event();
        launch = update_players(&player_units, &map) && launch;
        launch = update_camera(&camera, map) && launch;
        launch = BLV_draw(renderer.renderer, map, &player_units, camera) && launch;

        // printf("time : %ld avec : %ld et %ld et %ld\r\n", current_time - past_time, fps, current_time, past_time);
        past_time = SDL_GetTicks();
    }

    // clean time
    SDL_bool clean_success = SDL_TRUE;
    clean_success = clean_success && clean_BLV_Renderer(&renderer);
    clean_success = clean_success && clean_BLV_window(&window);
    clean_success = clean_success && clean_map(&map);

    if (clean_success == SDL_FALSE)
        SDL_Log("Clean operation failed");
    else
        SDL_Log("Clean operation success");

    IMG_Quit();
    SDL_Quit();

    // its okay
    return 0;
}

SDL_bool BLV_draw(SDL_Renderer *renderer, Map m, Player_array *p, Camera c)
{
    if (renderer == NULL)
    {
        SDL_Log("Warning can't draw thing on screen, cause renderer is NULL");
        return SDL_TRUE;
    }

    if (set_renderer_draw_color(renderer, color_black) == SDL_FALSE)
        return SDL_FALSE;

    if (SDL_RenderClear(renderer) < 0)
    {
        SDL_Log("Failed to SDL_RenderClear, cause %s", SDL_GetError());
        return SDL_FALSE;
    }

    /*SDL_Rect pos = {0,0, 1080, 720};
    if(draw_texture(renderer, bck_texture, pos) == SDL_FALSE)
    {
        SDL_Log("Failed to draw background texture on screen");
        return SDL_FALSE;
    }*/

    if (draw_map(renderer, m, c.pos) == SDL_FALSE)
    {
        SDL_Log("Failed to draw map on screen");
        return SDL_FALSE;
    }

    if (draw_players(renderer, p, c.pos, m) == SDL_FALSE)
    {
        SDL_Log("Failed to draw player on screen");
        return SDL_FALSE;
    }

    if (draw_camera(renderer, c) == SDL_FALSE)
    {
        SDL_Log("Failed to draw camera on screen");
        return SDL_FALSE;
    }

    SDL_RenderPresent(renderer);

    return SDL_TRUE;
}