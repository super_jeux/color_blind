/**
 * \brief contain all code definition to manage a map
 *
 * \file map.h
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#ifndef __MAP_H__
#define __MAP_H__

#include "BLV_geometry.h"
#include "BLV_draw.h"

/**
 * \brief use to define a map on screen
 */
typedef struct
{
    uint16_t offset_x;      /**<offset_x> start of left upper corner of map*/
    uint16_t offset_y;      /**<offset_y> start of right upper corner of map*/
    BLV_Point *map;         /**<map> current map to print*/
    BLV_Point *drawing_map; /**<drawing_map> current map with drawing position on screen*/
    uint16_t width;         /**<width> width of map*/
    uint16_t height;        /**<height> height of map*/
} Map;

/**
 * \brief use to initialize a map
 *
 * \param m pointer to map to fill
 * \param offset_x field of map
 * \param offset_y field of map
 * \param width field of map
 * \param height field of map
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool init_map(Map *m, uint16_t offset_x, uint16_t offset_y, uint16_t width, uint16_t height);

/**
 * \brief draw map on screen
 *
 * \param renderer where we draw on screen
 * \param m current map to draw
 * \param r camera
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_map(SDL_Renderer *renderer, Map m, BLV_Rect r);

/**
 *\brief use to desalocate allocated map
 *
 *\param m pointer to map to free
 *
 *\return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool clean_map(Map *m);

#endif
