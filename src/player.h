/**
 * \brief contain all code definition to manage a player
 *
 * \file player.h
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "BLV_geometry.h"
#include "BLV_draw.h"
#include "map.h"

/**
 * \brief use to define how many maximum unit are present in array of unit
 */
#define MAX_UNIT 255

/**
 * \brief use to define a player on screen
 */
typedef struct
{
    BLV_Rect pos;         /**<pos> pos of player in memory*/
    BLV_Rect drawing_pos; /**<drawing_pos> where player is draw on screen*/
    BLV_Rect contour;     /**<contour> contour of his drawing rect*/
    int8_t speed_x;       /**<speed_x> definie x speed*/
    int8_t speed_y;       /**<speed_y> define y speed*/
} Player;

/**
 * \brief use to represent how many unit we will have on battlefield
 */
typedef struct
{
    uint8_t unit_count;    /**<unit_count> how many unit present in array*/
    Player unit[MAX_UNIT]; /**<unit> array of unit*/
} Player_array;

/**
 * \brief use to init a player
 *
 * \param p pointer to player to fill
 * \param pos_x x coordinate of player
 * \param pos_y y coordinate of player
 * \param offset_x offset_x of map drawing start
 * \param offset_y offset_y of map drawing start
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool init_player(Player *p, uint16_t pos_x, uint16_t pos_y, uint16_t offset_x, uint16_t offset_y);

/**
 * \brief use to init an array of player
 *
 * \param array_p array of player fill
 * \param max_unit count of unit need to place on map
 * \param m map of battlefield
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool init_array_of_player(Player_array *array_p, uint8_t max_unit, Map m);

/**
 * \brief use to update a player
 *
 * \param p pointer to players to update
 * \param m pointer to map to update when we move
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool update_players(Player_array *p_array, Map *m);

/**
 * \brief use to draw player on screen
 *
 * \param renderer renderer where to draw player
 * \param p players to draw
 * \param cam_position position of camera on map
 * \param m map of game
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_players(SDL_Renderer *renderer, Player_array *p_array, BLV_Rect cam_position, Map m);

/**
 * \brief use to know if player is escaping map limit
 *
 * \param p player to check
 * \param m map to check
 */
void check_map_escape(Player *p, Map *m);

/**
 * \brief use to change player speed x and y
 *
 * \param p pointer to player
 * \param code keyboard event
 * \param state if button is pressed or released
 */
void change_player_speed(Player *p, SDL_Keycode code, Uint8 state);

#endif