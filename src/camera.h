/**
 * \brief file contain all code definition for manage a camera
 *
 * \file camera.h
 * \author Gautier Levesque
 * \date 06-02-2024
 */
#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "BLV_geometry.h"
#include "player.h"
#include "map.h"

/**
 * \brief use to define a camera
 */
typedef struct
{
    BLV_Rect pos;      /**<pos> current position of camer on map*/
    BLV_Rect collider; /**<collider> to know when we need to move camera*/
    int speed_x;       /**<speed_x> current speed x of camera*/
    int speed_y;       /**<speed_y> current speed y of camera*/
} Camera;

/**
 * \brief use to init a camera
 *
 * \param c camera to fill
 * \param screen_width current width of screen
 * \param screen_height current height of screen 
 * \param offset_x x offset coordinate where to start draw map on screen
 * \param offset_y y offset coordinate where to start draw map on screen
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool init_camera(Camera *c, uint16_t screen_width, uint16_t screen_height, uint16_t offset_x, uint16_t offset_y);

/**
 * \brief use to update camera speed and pos depending of player
 *
 * \param c pointer to camera to update
 * \param m map
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool update_camera(Camera *c, Map m);

/**
 * \brief use to draw camera on screen
 * 
 * \param renderer pointer to renderer where to draw camera
 * \param c camera to draw
 * 
 * \return SDL_TRUE if success, SDL_FALSE instead
*/
SDL_bool draw_camera(SDL_Renderer *renderer, Camera c);

#endif