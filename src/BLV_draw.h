/**
 * \brief contain all code definition to draw all thing on screen
 *
 * \file BLV_draw.h
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#ifndef __BLV_DRAW_H__
#define __BLV_DRAW_H__

#include <SDL.h>

#include "BLV_geometry.h"
#include "BLV_graphic.h"

/**
 * \brief use to set a color on a SDL_Renderer
 *
 * \param renderer renderer where to set SDL_Color
 * \param color current color to set on SDL_Renderer
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool set_renderer_draw_color(SDL_Renderer *renderer, SDL_Color color);

/**
 * \brief use to draw a SDL_point with a color on a SDL_Renderer
 *
 * \param renderer renderer where to draw SDL_Point
 * \param point current point to draw
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_point(SDL_Renderer *renderer, BLV_Point point);

/**
 * \brief use to draw a line with a color on a SDL_Renderer
 *
 * \param renderer renderer where to draw line
 * \param line current line to draw
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_line(SDL_Renderer *renderer, BLV_Line line);

/**
 * \brief use to draw a SDL_rect with a color on a SDL_Renderer
 *
 * \param renderer renderer where to draw SDL_rect
 * \param rect current rect to draw
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_rect(SDL_Renderer *renderer, BLV_Rect rect);

/**
 * \brief use to draw a texture on screen
 *
 * \param renderer renderer where to draw texture
 * \param texture texture to draw
 * \param dst where to draw on screen
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_texture(SDL_Renderer *renderer, BLV_Texture texture, SDL_Rect dst);

/**
 * \brief use to draw a texture on screen with rotation
 *
 * \param renderer renderer where to draw texture
 * \param texture texture to draw
 * \param dst where to draw on screen
 * \param angle angle of rotation
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool draw_textureEx(SDL_Renderer *renderer, BLV_Texture texture, SDL_Rect dst, double angle);

#endif