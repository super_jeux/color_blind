/**
 * \brief contain all code implementation to manage a player
 *
 * \file player.c
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#include "player.h"

#include <string.h>
#include <unistd.h>

SDL_bool init_player(Player *p, uint16_t pos_x, uint16_t pos_y, uint16_t offset_x, uint16_t offset_y)
{
    SDL_Color contour = {255, 255, 255, 255};
    SDL_Color p_color = {255, 0, 0, 255};
    SDL_Rect ppos = {pos_x, pos_y, 50, 50};
    SDL_Rect c = {pos_x, pos_y, 50, 50};

    memcpy(&p->pos.r, &ppos, sizeof(SDL_Rect));
    memcpy(&p->pos.color, &p_color, sizeof(SDL_Color));
    memcpy(&p->contour.r, &c, sizeof(SDL_Rect));
    memcpy(&p->contour.color, &contour, sizeof(SDL_Color));
    p->pos.fill = SDL_TRUE;
    p->contour.fill = SDL_FALSE;
    memcpy(&p->drawing_pos, &p->pos, sizeof(BLV_Rect));
    p->drawing_pos.r.x += offset_x / 2;
    p->drawing_pos.r.y += offset_y / 2;
    p->contour.r.x = p->drawing_pos.r.x;
    p->contour.r.y = p->drawing_pos.r.y;

    return SDL_TRUE;
}

SDL_bool init_array_of_player(Player_array *array_p, uint8_t max_unit, Map m)
{
    memset(array_p, 0, sizeof(Player_array));
    for (uint8_t i = 0; i < max_unit; ++i)
    {
        usleep(25);
        uint16_t pos_x = rand() % 500;
        uint16_t pos_y = rand() % m.height;

        if (init_player(&array_p->unit[i], pos_x, pos_y, m.offset_x, m.offset_y) == SDL_FALSE)
            return SDL_FALSE;

        printf("Pos : %d,%d\r\n", pos_x, pos_y);
        array_p->unit_count += 1;
    }

    return SDL_TRUE;
}

SDL_bool update_players(Player_array *p_array, Map *m)
{
    for (uint8_t i = 0; i < p_array->unit_count; ++i)
    {
        Player *p = &p_array->unit[i];
        if (p->speed_x != 0 || p->speed_y != 0)
        {
            p->pos.r.x += p->speed_x;
            p->pos.r.y += p->speed_y;

            check_map_escape(p, m);

            for (uint16_t y = p->pos.r.y; y < p->pos.r.y + p->pos.r.h; ++y)
            {
                for (uint16_t x = p->pos.r.x; x < p->pos.r.x + p->pos.r.w; ++x)
                {
                    m->map[y * m->width + x].color.r = p->pos.color.r;
                    m->map[y * m->width + x].color.g = p->pos.color.g;
                    m->map[y * m->width + x].color.b = p->pos.color.b;
                    m->drawing_map[y * m->width + x].color.r = p->pos.color.r;
                    m->drawing_map[y * m->width + x].color.g = p->pos.color.g;
                    m->drawing_map[y * m->width + x].color.b = p->pos.color.b;
                }
            }
        }
    }

    return SDL_TRUE;
}

SDL_bool draw_players(SDL_Renderer *renderer, Player_array *p_array, BLV_Rect cam_position, Map m)
{
    for (uint8_t i = 0; i < p_array->unit_count; ++i)
    {
        Player *p = &p_array->unit[i];
        if (SDL_IntersectRect(&p->pos.r, &cam_position.r, &p->drawing_pos.r) == SDL_FALSE)
            continue;

        p->drawing_pos.r.x += (m.offset_x / 2) - cam_position.r.x;
        p->drawing_pos.r.y += (m.offset_y / 2) - cam_position.r.y;
        if (draw_rect(renderer, p->drawing_pos) == SDL_FALSE)
            return SDL_FALSE;

        p->contour.r.x = p->drawing_pos.r.x;
        p->contour.r.y = p->drawing_pos.r.y;
        p->contour.r.w = p->drawing_pos.r.w;
        p->contour.r.h = p->drawing_pos.r.h;

        if (draw_rect(renderer, p->contour) == SDL_FALSE)
            return SDL_FALSE;
    }

    return SDL_TRUE;
}

void check_map_escape(Player *p, Map *m)
{
    if (p->pos.r.x < 0)
        p->pos.r.x = 0;
    if (p->pos.r.x + p->pos.r.w >= m->width)
        p->pos.r.x = m->width - p->pos.r.w;
    if (p->pos.r.y < 0)
        p->pos.r.y = 0;
    if (p->pos.r.y + p->pos.r.w >= m->height)
        p->pos.r.y = m->height - p->pos.r.h;
}

void change_player_speed(Player *p, SDL_Keycode code, Uint8 state)
{
    switch (code)
    {
    case SDLK_z:
        p->speed_y = state == SDL_PRESSED ? -10 : 0;
        break;
    case SDLK_s:
        p->speed_y = state == SDL_PRESSED ? 10 : 0;
        break;
    case SDLK_q:
        p->speed_x = state == SDL_PRESSED ? -10 : 0;
        break;
    case SDLK_d:
        p->speed_x = state == SDL_PRESSED ? 10 : 0;
        break;
    case SDLK_UP:
        p->speed_y = state == SDL_PRESSED ? -10 : 0;
        break;
    case SDLK_DOWN:
        p->speed_y = state == SDL_PRESSED ? 10 : 0;
        break;
    case SDLK_LEFT:
        p->speed_x = state == SDL_PRESSED ? -10 : 0;
        break;
    case SDLK_RIGHT:
        p->speed_x = state == SDL_PRESSED ? 10 : 0;
        break;
    case SDLK_w:
        p->speed_y = state == SDL_PRESSED ? -10 : 0;
        break;
    case SDLK_a:
        p->speed_x = state == SDL_PRESSED ? -10 : 0;
        break;
    }
}