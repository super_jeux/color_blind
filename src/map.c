/**
 * \brief contain all code implementation to manage a map
 *
 * \file map.c
 * \author Gautier Levesque
 * \date 03-02-2024
 */
#include "map.h"

#include <string.h>

SDL_bool init_map(Map *m, uint16_t offset_x, uint16_t offset_y, uint16_t width, uint16_t height)
{
    SDL_Log("Try to initialize map");
    if (m == NULL)
    {
        SDL_Log("Warning we won't initialize map, cause map is NULL");
        return SDL_TRUE;
    }
    memset(m, 0, sizeof(Map));

    m->height = height;
    m->offset_x = offset_x;
    m->offset_y = offset_y;
    m->width = width;

    m->map = malloc(sizeof(BLV_Point) * width * height);
    m->drawing_map = malloc(sizeof(BLV_Point) * width * height);
    if (m->map == NULL || m->drawing_map == NULL)
    {
        SDL_Log("Failed to initialize map, cause malloc failed");
        return SDL_FALSE;
    }

    SDL_Color default_color = {255, 255, 255, 200};
    for (uint16_t y = 0; y < height; ++y)
    {
        for (uint16_t x = 0; x < width; ++x)
        {
            SDL_Point default_point = {x, y};
            memcpy(&m->map[y * width + x].color, &default_color, sizeof(SDL_Color));
            memcpy(&m->map[y * width + x].p, &default_point, sizeof(SDL_Point));
            memcpy(&m->drawing_map[y * width + x], &m->map[y * width + x], sizeof(BLV_Point));
        }
    }

    SDL_Log("initialize map success");
    return SDL_TRUE;
}

SDL_bool draw_map(SDL_Renderer *renderer, Map m, BLV_Rect r)
{
    uint16_t y_border = r.r.y + r.r.h > m.height ? m.height : r.r.y + r.r.h;
    uint16_t x_border = r.r.x + r.r.w > m.width ? m.width : r.r.x + r.r.w;


    for (uint16_t y = r.r.y; y < y_border; ++y)
    {
        for (uint16_t x = r.r.x; x < x_border; ++x)
        {
            m.drawing_map[y * m.width + x].p.x = m.map[y * m.width + x].p.x - r.r.x + m.offset_x / 2;
            m.drawing_map[y * m.width + x].p.y = m.map[y * m.width + x].p.y - r.r.y + m.offset_y / 2;
            //printf("pos : %d;%d\r\n", m.drawing_map[y * m.width + x].p.x, m.drawing_map[y * m.width + x].p.y);

            if (draw_point(renderer, m.drawing_map[y * m.width + x]) == SDL_FALSE)
                return SDL_FALSE;
        }
    }

    return SDL_TRUE;
}

SDL_bool clean_map(Map *m)
{
    SDL_Log("Try to clean a Map");

    if (m == NULL)
    {
        SDL_Log("Failed to clean a Map, cause map is NULL");
        return SDL_FALSE;
    }

    if (m->map != NULL)
    {
        free(m->map);
        m->map = NULL;
    }
    else
        SDL_Log("WARNING : Clean Map ok but m->map is NULL");

    if (m->drawing_map != NULL)
    {
        free(m->drawing_map);
        m->drawing_map = NULL;
    }
    else
        SDL_Log("WARNING : Clean Map ok but m->drawing_map is NULL");

    memset(m, 0, sizeof(Map));

    SDL_Log("Clean Map success");
    return SDL_TRUE;
}