/**
 * \brief file contain all code definition for launch and manage a SDL_Renderer and texture with SDL2
 *
 * \file BLV_graphic.h
 * \author Gautier Levesque
 * \date 28-01-2024
 */

#ifndef __BLV_GRAPHIC_H__
#define __BLV_GRAPHIC_H__

#include <SDL.h>

/**
 * \brief Max count of SDL_Texture
 */
#define MAX_TEXTURE 500

/**
 * \brief Max caractere for a texture name
 */
#define MAX_CAR_TEXTURE_NAME 255

/**
 * \brief use to define a SDL_Texture
 */
typedef struct
{
    SDL_Texture *texture;            /**<texture> current SDL_Texture*/
    SDL_Rect src;                    /**<src> source of texture*/
    SDL_RendererFlip flip;           /**<flip> flip mode horizontal of vertical*/
    SDL_Point center;                /**<center> center of texture*/
    char name[MAX_CAR_TEXTURE_NAME]; /**<name> name of texture*/
} BLV_Texture;

/**
 * \brief represent a SDL_Renderer with some parameter
 */
typedef struct
{
    SDL_Renderer *renderer;                 /**<renderer> current SDL_Renderer*/
    int idx;                                /**<idx> index of rendering driver*/
    Uint32 flags;                           /**<flags> flags of SDL_Renderer*/
    uint16_t texture_count;                 /**<texture_count> number of SDL_Texture in texture_array*/
    BLV_Texture texture_array[MAX_TEXTURE]; /**<texture_array>  array of all texture*/
    SDL_bool is_renderer_dirty;             /**<is_renderer_dirty> if we need to redraw screen*/
} BLV_Renderer;

/**
 * \brief use to create a BLV_Renderer
 *
 * \param w pointer to SDL_Window use to attach SDL_Renderer to SDL_Window
 * \param r pointer to a SDL_Renderer who will be filled in this function
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool create_BLV_renderer(SDL_Window *w, BLV_Renderer *r);

/**
 * \brief use to create a texture
 *
 * \param path path to texture on disk
 * \param renderer renderer use
 * \param texture texture to filled
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool create_texture(const char *path, SDL_Renderer *renderer, SDL_Texture **texture);

/**
 * \brief use to create all needed SDL_Texture
 *
 * \param path path to directory where we can found all SDL_Texture
 * \param r BLV_Renderer created
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool create_all_texture(const char *path, BLV_Renderer *r);

/**
 * \brief use to clean a BLV_Renderer
 *
 * \param r pointer to BLV_Renderer who will be clean
 *
 * \return SDL_TRUE if success, SDL_FALSE instead
 */
SDL_bool clean_BLV_Renderer(BLV_Renderer *r);

#endif